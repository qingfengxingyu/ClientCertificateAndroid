package com.xingyu.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.xingyu.app.utils.HttpMananger;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private TextView getTxt, postTxt, postTxt3;
    private ProgressDialog mDefaultDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        getTxt = findViewById(R.id.getTxt);
        postTxt = findViewById(R.id.postTxt);
        postTxt3 = findViewById(R.id.postTxt3);
        mDefaultDialog = new ProgressDialog(this);
        mDefaultDialog.setMessage("正在进行网络请求...");
        mDefaultDialog.setCanceledOnTouchOutside(false);//默认true
        findViewById(R.id.getBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mDefaultDialog) {
                    mDefaultDialog.show();
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String resutl = HttpMananger.getInstance().getHttp("https://192.168.43.91:443/test/admin");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mDefaultDialog) {
                                    mDefaultDialog.cancel();
                                }
                                getTxt.setText(resutl);
                            }
                        });
                    }
                }).start();
            }
        });
        findViewById(R.id.postBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mDefaultDialog) {
                    mDefaultDialog.show();
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("abc", "测试");
                            obj.put("asd", "hello");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        final String resutl = HttpMananger.getInstance().postJsonHttp("https://192.168.43.91:443/test/qwe", obj.toString(), MainActivity.this);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mDefaultDialog) {
                                    mDefaultDialog.cancel();
                                }
                                postTxt.setText(resutl);
                            }
                        });
                    }
                }).start();
            }
        });
        findViewById(R.id.postBtn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mDefaultDialog) {
                    mDefaultDialog.show();
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("abc", "测试");
                            obj.put("asd", "hello");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
//                        final String resutl = HttpMananger.getInstance().postOkHttps("https://192.168.43.91:443/test/qwe", obj.toString(),MainActivity.this);
                        final String resutl = HttpMananger.getInstance().postOkHttps("https://192.168.43.91:443/test/qwe", obj.toString(),MainActivity.this);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mDefaultDialog) {
                                    mDefaultDialog.cancel();
                                }
                                postTxt3.setText(resutl);
                            }
                        });
                    }
                }).start();
            }
        });
    }
}
