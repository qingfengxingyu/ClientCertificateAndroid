package com.xingyu.app.utils.https;


import android.content.Context;

import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;

public class SSLHttpClient {
    /**
     * 发送https请求
     * 为双向请求
     *
     * @return
     */
    public static HttpClient getHttps(Context mContext) {
        // 服务器端需要验证的客户端证书
        KeyStore Server_keyStore = null;
        // 客户端信任的服务器端证书
        KeyStore Client_keyStore = null;
        InputStream Server_In = null;
        InputStream Client_In = null;
        try {
            Server_keyStore = KeyStore.getInstance(CertificateConfig.KEY_STORE_TYPE_P12);
            Client_keyStore = KeyStore.getInstance(CertificateConfig.KEY_STORE_TYPE_BKS);
            Server_In = mContext.getResources().getAssets()
                    .open(CertificateConfig.KEY_STORE_CLIENT_PATH);
            Client_In = mContext.getResources().getAssets()
                    .open(CertificateConfig.KEY_STORE_TRUST_PATH);
            Server_keyStore.load(Server_In, CertificateConfig.KEY_STORE_PASSWORD.toCharArray());
            Client_keyStore.load(Client_In, CertificateConfig.KEY_STORE_TRUST_PASSWORD.toCharArray());
            //设置规则限制
            SSLSocketFactory socketFactory = new SSLSocketFactory(Server_keyStore, CertificateConfig.KEY_STORE_PASSWORD, Client_keyStore);
            socketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            //注册
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", socketFactory, 443));
            //HttpClient设置
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            HttpProtocolParams.setUseExpectContinue(params, true);
            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    params, registry);
            return new DefaultHttpClient(ccm, params);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (null != Server_In) {
                    Server_In.close();
                }
                if (null != Client_In) {
                    Client_In.close();
                }
            } catch (IOException io) {
                io.printStackTrace();
            }
        }
        return null;
    }

}