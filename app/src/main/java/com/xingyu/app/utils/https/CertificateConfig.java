package com.xingyu.app.utils.https;

/**
 * author:xmf
 * date:2018/10/24 0024
 * description:证书默认参数存放
 */
public class CertificateConfig {
    public static final String KEY_STORE_TYPE_BKS = "bks";
    public static final String KEY_STORE_TYPE_P12 = "PKCS12";
    public static final String KEY_STORE_TRUST_PATH = "tomatocc.truststore";//truststore文件
    public static final String KEY_STORE_CLIENT_PATH = "tomatocc.p12";//P12文件
    public static final String KEY_STORE_PASSWORD = "123456";//P12文件密码
    public static final String KEY_STORE_TRUST_PASSWORD = "123456";//truststore文件密码
}
