package com.xingyu.app.utils.https;

import android.content.Context;

import java.io.InputStream;
import java.security.KeyStore;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.OkHttpClient;

/**
 * author:xmf
 * date:2018/10/16 0016
 * description:Okhttp访问方式的https双向验证请求
 */
public class SSLOkhttp {

    public static SSLSocketFactory getSSLSocketFactory(Context mContext) {
        SSLSocketFactory factory = null;
        // 服务器端需要验证的客户端证书
        KeyStore Server_keyStore = null;
        // 客户端信任的服务器端证书
        KeyStore Client_keyStore = null;
        InputStream Server_In = null;
        InputStream Client_In = null;
        try {
            Server_keyStore = KeyStore.getInstance(CertificateConfig.KEY_STORE_TYPE_P12);
            Client_keyStore = KeyStore.getInstance(CertificateConfig.KEY_STORE_TYPE_BKS);
            Server_In = mContext.getResources().getAssets()
                    .open(CertificateConfig.KEY_STORE_CLIENT_PATH);
            Client_In = mContext.getResources().getAssets()
                    .open(CertificateConfig.KEY_STORE_TRUST_PATH);
            Server_keyStore.load(Server_In, CertificateConfig.KEY_STORE_PASSWORD.toCharArray());
            Client_keyStore.load(Client_In, CertificateConfig.KEY_STORE_TRUST_PASSWORD.toCharArray());
            //信任管理器
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(Client_keyStore);
            //密钥管理器
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("X509");
            keyManagerFactory.init(Server_keyStore, CertificateConfig.KEY_STORE_PASSWORD.toCharArray());
            //初始化SSLContext
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(),
                    trustManagerFactory.getTrustManagers(), null);
            factory = sslContext.getSocketFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return factory;
    }

    public static OkHttpClient getHttps(Context mContext) {
        try {
            OkHttpClient.Builder buider = new OkHttpClient.Builder();
            buider.connectTimeout(20, TimeUnit.SECONDS);
            buider.writeTimeout(20, TimeUnit.SECONDS);
            buider.readTimeout(90, TimeUnit.SECONDS);
            return getHttps(mContext, buider);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static OkHttpClient getHttps(Context mContext, OkHttpClient.Builder buider) {
        try {
            final HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(final String hostname,
                                      final SSLSession session) {
                    System.out.println("hostname = [" + hostname + "],session = [" + session.getPeerHost() + "]");
//                    if (hostname.equals("地址") || session.equals("地址"))
//                        return true;
//                    else
//                        return false;
                    return true;
                }
            };
            buider.hostnameVerifier(hostnameVerifier);
            buider.sslSocketFactory(getSSLSocketFactory(mContext));
            return buider.build();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
