package com.xingyu.app.utils;

import org.apache.http.HttpEntity;
import org.apache.http.util.CharArrayBuffer;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * author:xmf
 * date:2018/10/23 0023
 * description:http相关的工具辅助类
 */
public class HttpUtils {
    public static byte[] getRawData(HttpEntity entity) {
        // 解析响应流
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        try {
            entity.writeTo(outStream);
            byte[] outBytes = outStream.toByteArray();
            return outBytes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 获得HttpEntity数据 默认转码格式UTF-8
     * @param entity
     * @return
     * @throws IOException
     */
    public static String entityToString(HttpEntity entity) throws IOException {
        return entityToString(entity, "UTF-8");
    }

    /***
     * 获得HttpEntity数据 默认转码格式UTF-8
     * @param entity 数据源
     * @param typeData  转码类型
     * @return
     * @throws IOException
     */
    public static String entityToString(HttpEntity entity, String typeData) throws IOException {
        if (null == typeData || "".equals(typeData)) {
            typeData = "UTF-8";
        }
        String result = null;
        if (entity != null) {
            long lenth = entity.getContentLength();
            if (lenth != -1 && lenth < 2048) {
                result = EntityUtils.toString(entity, typeData);
            } else {
                InputStreamReader reader1 = new InputStreamReader(entity.getContent(), typeData);
                CharArrayBuffer buffer = new CharArrayBuffer(2048);
                char[] tmp = new char[1024];
                int l;
                while ((l = reader1.read(tmp)) != -1) {
                    buffer.append(tmp, 0, l);
                }
                result = buffer.toString();
            }
        }
        return result;
    }
}
