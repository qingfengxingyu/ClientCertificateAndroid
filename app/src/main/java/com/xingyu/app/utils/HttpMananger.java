package com.xingyu.app.utils;

import android.content.Context;
import android.util.Log;

import com.xingyu.app.utils.https.SSLOkhttp;
import com.xingyu.app.utils.https.SSLHttpClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import java.io.IOException;

import javax.net.ssl.SSLHandshakeException;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class HttpMananger {
    private final String EORROR = "----------------https 握手失败------------------";

    private HttpMananger() {
    }

    private int _timeout = 15000;

    private static class Holder {
        private static HttpMananger mInstance = new HttpMananger();
    }

    public static HttpMananger getInstance() {
        return Holder.mInstance;
    }

    /***
     * HttpClent的Get请求
     * @param url
     * @return
     */
    public String getHttp(String url) {
        String result = null;
        HttpClient httpClient = null;
        HttpResponse response = null;
        try {
            httpClient = new DefaultHttpClient();
            httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, _timeout);
            httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, _timeout);
            HttpGet get = new HttpGet(url);
            response = httpClient.execute(get);
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                result = HttpUtils.entityToString(entity);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * HttpClent的post请求，参数为json字符串
     *
     * @param url        请求地址
     * @param jsonString json字符串
     * @return 响应
     */
    public String postJsonHttp(String url, String jsonString, Context mContext) {
        String result = null;
        HttpClient httpClient = null;
        HttpResponse response = null;
        try {
            httpClient = SSLHttpClient.getHttps(mContext);
            httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, _timeout);
            httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, _timeout);
            HttpPost post = new HttpPost(url);
            post.setEntity(new ByteArrayEntity(jsonString.getBytes("UTF-8")));
            response = httpClient.execute(post);
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                result = HttpUtils.entityToString(entity);
            }
            return result;
        } catch (Exception e) {
            if (e instanceof SSLHandshakeException) {
                Log.e("xmf", EORROR);
            }
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 使用Okhttp进行双向验证的请求接口
     * @param url
     * @param jsonString
     * @param mContext
     * @return
     */
    public String postOkHttps(String url, String jsonString, Context mContext) {
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, jsonString);
            Request request = new Request.Builder()
                    .url(url).post(body).build();
            Response response = SSLOkhttp.getHttps(mContext).newCall(request).execute();
            if (null != response) {
                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    return responseBody.string();
                }
            }
        } catch (Exception ex) {
            if (ex instanceof SSLHandshakeException) {
                Log.e("xmf", EORROR);
            }
            ex.printStackTrace();
        }
        return "";
    }
}
